# Input video: traffic_input.mp4
The traffic video show the demand distribution in the map for the first 150 time_id of the training.csv data.
Brighter color indicate higher demand.

# Output video: traffic_output_exp<model_number>_<num_epoch>.mp4
The output video show comparison between the predicted output and the true output.
It is divided into 3 rows and 5 columns of images.

The first row show the predicted output.
The second row show the true output.
The last row show the difference between the predicted output and the true output.

For the columns, the column C represent demand at time T+C.
The first column show demand at time T+1, the second column show demand at time T+2, etc.

Similar with the input video, brighter color indicate higher demand.

