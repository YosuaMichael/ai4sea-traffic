# AI4Sea -- Traffic Management

## Introduction
This repository contains code that experimenting with traffic management problem , one of the challenge in AI4Sea ([Click here for detail](https://www.aiforsea.com/traffic-management)). The codes is written in a notebook traffic.ipynb. We also provide a video in the video folder that visualize some of the model result.

## Setup
The code in the notebook use python 3.6.5. The python packages is documented in *requirements.txt* file.
To install the python packages use the following command:

> pip install -r requirements.txt

We used keras with tensorflow backend for the model in the experiment. A tensorflow 1.10.1 with GPU in CUDA 9.0 with OS Ubuntu 18.04 during the experiment, however other version should be compatible.
Installation of CUDA, tensorflow, and keras are required to run the script. Since this installation method may differ depending on the OS used, I suggest to google on how to install them.

For visualization, we mostly save the video using ffmpeg library in ubuntu 18.04, it can be installed with command:

> sudo apt install ffmpeg

## The Notebook Script
Once all the requirements is installed, we can open jupyter notebook or lab to open traffic.ipynb. 
The code is designed to be run sequentially, however the user can directly jump to production code and run it if required.

The code has the following content:

### Library Imports and Notebook Setup
This part basically just import the library and setting up notebook

### Training Data Download
We do not want to put the training data in the repository since it have a big size (it is a bad practice to put a big file inside a repository). Hence as alternative, we create a code in python that can download the training data. It basically download the zip file from https://s3-ap-southeast-1.amazonaws.com/grab-aiforsea-dataset/traffic-management.zip , unzip the file, and move the csv file inside to the root folder of the repository.

### Preprocess and Analysis Data
In the preprocessing and analysis data part, we try to see and make sense of the input data. On some part we did some preprocessing that we think is necessary. At the end of this part, we convert from the tabular raw input data into images data that represent the demand for each time slot.

### Experiment Preparation Code
In here we basically import the keras libraries and create some helper function for the experiment. 

### Generate Input and Output for the Model
Although at the end of preprocessing steps we already have the data as numpy images, it is still not ready to be used directly by the model in Keras. In this steps, we create a ready-to-use input and output data. We also separate the data into training, testing, and validation data with ratio **0.8 : 0.1 : 0.1** respectively.

### Model Definition and Experiments
In this part, we define the models for our experiments. We document 3 different experiments each of them has their own model. All the model is using neural network in Keras with tensorflow backend. Some brief overview of  each experiments:

 1. Experiment 1: Use Convolutional Auto Encoder Look Alike
 2. Experiment 2: Similar with Model 1, but now the autoencoder predict the changes in demand instead of the demand itself
 3. Experiment 3: Add time information as input to the model

Personally, I have tried some other experiments. But most of them don't really give significant improvement. Experiment 1 is kinda the baseline,  experiment 2 give some improvement, then experiment 3 give further improvement compared to experiment 2.

### Model Training and Evaluation
Once we defined all the model, in this part of the code, user can choose which model they want to train and evaluate. The user choose by uncomment the code for the experiment they want to choose. Once they choose, they can procede the code by compiling, training, visualizing the prediction, and lastly evaluate the model on validation data.

### Production Code
The production code is created after we are done with the experiment. It is easier to reuse and have functionality such as save-load model, and predicting future 5 slot (75 minutes) of the input csv.

If the evaluator want to evaluate the model, we suggest to use the production code.
If there are difficulties in training the model, we also provide a pretrained model in: [Click here](https://drive.google.com/file/d/1NuhyqleBw9lZ0odmr_wiVo_d1F_bgnKp/view?usp=sharing).
The pretrained model can be downloaded and saved as model/traffic.h5 in the repository.

### Possible Future Improvement
This part describe some possible things to do in the future to improve the model.

### Result and Summary
Finally we describe some result that we got from experiment in here.

## Notes for Evaluation
To evaluate the model, the evaluator is recommended to use the Production code section.

In the production code we provide a TrafficManagement class that represent our model with some additional useful other functionality.
We also provide 2 main functions as example, the evaluator may changes some part of it for evaluation.

The first main functions basically train a model and save the model to a file.

The second main functions load a model and evaluate the performance of the model. After that it also do a prediction of the future T+1, ..., T+5 after the input and save it as a csv file.
Evaluator may changes the evaluation data in this main functions (since we dont have any other csv, we use training.csv as evaluation data, but evaluator may use other csv).


## Author
Yosua Michael Maranatha

